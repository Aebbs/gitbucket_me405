##  @file ProjectProposal.py
#  This is the Project Proposal for ME 450 <br>
#
#
#  @mainpage
#  This is the Project Proposal for ME 450 <br>
#  <a href="https://aebbs.bitbucket.io/htmlLab2/index.html">Click HERE to return to the project Mainpage</a> 
#  \tableofcontents
#
#  @section sec_prob Problem Statement
#  The objective of my project is to create a 2-axis gimbal to stabilize a phone for taking pictures or video.
#  The gimbal will stabilize rotation in the pitch and roll directions, and will allow the user some manual control
#  over the motors.<br>
#  
#
#
#  @subsection sec_req Project Requirement
#  My project will use 2 DC motors in closed loop control: this fulfills the requirement for 2 actuators. <br>
#  My project will use 2 motor encoders, and an IMU: this fulfills the requirement for 2 sensors. <br>
#  My project will use an analog joystick: this fulfills the requirement for one new sensor. <br>
#  Both motors will run in real time based on feedback from the various sensors. <br>
#
#
#  @subsection sec_mats Materials
#  I already have access to the 2 DC motors, IMU, and Nucleo from the 450 kit.
#  I have purchased several options for the joystick sensor. All external parts including gears and
#  housings will be 3-D printed using my personal printer. Bearings and other components have been located around
#  my house. A mock phone may be used to reduce the overall size and weight of the device to comply with
#  the small motors that I plan to use. A initial model of the gimbal enclosure was created in solidworks
#  and is shown below:
#  @image html solidworksGimbal.PNG
#
#  @subsection sec_manu Manufacturing Plan
#  The entire project will be 3-D printed, and the assembly of components will
#  require only glue and interference fits. I already posses the 3-D printer and resin needed to print the parts.
#
#
#  @subsection sec_saf Safety:
#  There are several small pinch points on the device, however the motors used in my project will stall
#  long before causing physical harm. There are electronic components similar to those used in labs 1-4
#  of the course: and consequently I will take similar safety precautions while handling the nucleo and motors. 
#
#
#  @subsection sec_tim Timeline:
#  5/20-5/22: Create initial model and project proposal. <br>
#  5/22-5/29: Print parts for initial model, write version 1 of code (not using external joystick). <br>
#  5/29-6/5: Revise model and test version 1. Write version 2 of code (including external joystick). <br>
#  6/5-6/12: Print and revised parts, refine code, write report. <br>
#  6/12: Project due. <br>

#
#  @date 5/22/2020
