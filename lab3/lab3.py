## @file lab3.py
#  This is the closed loop proportional controller for the ME 450 code
#
#
#  @mainpage
#  \tableofcontents
#
#  @section sec_intro Introduction
#  This file contains the P_Controller_CL Class, which represents the work done in "lab3."
#  for the class ME 405. <br>
#  <a href="https://aebbs.bitbucket.io/mainpage/">CLICK HERE TO RETURN TO THE PROJECT MAINPAGE</a>
#
#
#  @subsection sec_drv Motor Controller
#  The P_Controller_CL Class creates a proportional, closed loop motor controller for the
#  nucleo development board. The class is initialized with a Kp value representing the proportional gain. <br>
#  update() is used to return an actuation signal to be used as a duty cycle.
#
#  @subsection sec_usg Usage
#  To create a motor controller object use: <br>
#  P_Controller_CL(Kp): <br>
#  With Kp being the proportional gain <br>
#  <br>
#  To return an actuation signal use: <br>
#  update (self, setpoint, actual_pos): <br>
#  With setpoint representing the desired position and actual_pos
#  representing the current position. It is reccomended update is called approximately every 10ms.
#
#  @subsection sec_tst Testing
#  This code was tested on the ME405 development board, and was used to run a series of step response tests. 
#  Please see the "Step Response Test" page in the documentation below for details on how the test was performed,
#  how the controller was tuned, and for step response plots.
#
#
#  @subsection sec_lim Limitations
#  Only Kp values below 0.5 were tested. Using a large Kp value may cause rapid and undesireable oscilation of the motor.
#
#  @copyright This software is open-source
#
#  @date 5/5/2020
#
#  @page page1 Step Response Test
#  This page details the step response tests that were performed using the closed loop, proportional motor controller
#  developed in lab 3.
#
#  @section sec Tests Performed
#  5 tests were performed using the motor controller, each with a different gain. Each test was run
#  with a setpoint of 5000 steps, and the motor was allowed to run for 3 seconds to ensure steady state was
#  reached. The tests used Kp = 0.01, Kp = 0.05, Kp = 0.1, Kp = 0.2, and Kp = 0.5.
#
#  @subsection Tuning Criteria 
#  The goal of the step response testing was to obtain a Kp value that resulted in the best response time without
#  overshoot.
#
#  @subsection Plots
#  Shown below are the 5 output plots, displaying the step response data collected for each test.
#  @image html plots.jpg
#  As shown in the plots above, the best tuning value for Kp was found to be Kp = 0.05 <br>
#  With a Kp of 0.05, the response time is approximately 500 ms, and the motor does not overshoot.
#
#  @subsection Testing Video
#  A video link of the step response using the final Kp = 0.05 is linked here.
#  https://youtu.be/MwCvMEBnrj8


import utime
import pyb
from lab2 import MotorEncoder
from lab1 import MotorDriver


class P_Controller_CL:
    ''' This class implements a closed loop proportional controller for the
    ME405 board. '''

    def __init__ (self, Kproportional):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param Kproportional A float value used to set the proportional gain of the controller.
        '''
        print ('Creating a closed loop proportional controller')
        self.Kp = Kproportional
        
          
    def update (self, setpoint, actual_pos):
        '''This function returns the actuation signal used to control the motor. The function
        takes in a setpoint, and actual position, with setpoint representing the desired position and actual_pos
        representing the current position. Update should be called approximately every 10ms. The returned actuation
        signal is between -100 and 100, and should be used as the motor's duty cycle.'''
        
        #define error as actual position - desired position
        error = actual_pos -setpoint
        
        #create an actuation signal based on the error and Kp. This represents the motor duty cycle
        act_signal = error * self.Kp
        
        #duty cycle must be between -100 and 100, so ensure -100<actuation signal<100
        if(act_signal > 100):
            act_signal = 100
        if(act_signal < -100):
            act_signal = -100
        
        return(act_signal)

        


if __name__ == '__main__':
    ''' description here'''
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create the pin objects used for interfacing with encoders
    pin_ENC1_A = pyb.Pin (pyb.Pin.cpu.B6)
    pin_ENC1_B =  pyb.Pin (pyb.Pin.cpu.B7)
    # Create the pin objects used for interfacing with the motor driver 
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10)
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4)
    pin_IN2 =  pyb.Pin (pyb.Pin.cpu.B5)
    
    # Create the timer objects used for encoder counting mode
    tim4 = pyb.Timer(4)
    # Create the timer object used for PWM generation
    tim1 = pyb.Timer(3) #setting frequency above audible range 
    
    
    # Create an encoder object, passing in the pins and timer
    Enc1 = MotorEncoder(pin_ENC1_A, pin_ENC1_B, tim4)
    # Create a motor object passing in the pins and timer
    Motor1 = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim1)
    
    setpoint = 5000
    
    while True:
        Motor1.enable()
        step_response = []
        time_data = []
        
        Kprop = input('Enter a Kp for the system: ')
        CLP1 = P_Controller_CL(float(Kprop))
        
        Enc1.update()
        Enc1.set_abs_position(0)
        
        t = 0
        t_init = utime.ticks_ms()
        
        while t<300:
            #update encoder position
            Enc1.update()
            currentPos = Enc1.get_abs_position()
            #append new data to data arrays
            time_data.append(utime.ticks_ms()-t_init)
            step_response.append(currentPos)
            
            #set Duty cycle to actuation signal
            act_sig = CLP1.update(setpoint,currentPos)
            Motor1.set_duty(act_sig)
            
            utime.sleep_ms(10)
            t = t+1
        
        Motor1.disable()
        for i in range(len(time_data)):
            print(time_data[i], ", ", step_response[i])
            
    
            
        
