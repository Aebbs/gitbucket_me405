## @file project.py
#  This is the project for ME 450 
#
#
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This file contains two classes"
#
#
#  @subsection sec_drv IMU
#  The IMU Class is used to interact with the DFRobot 9dof IMU provided in the ME 405 kit.
#  The class allows the user to enable and disable the imu, change the opperating mode,
#  and obtain various data including Euler Angles and angular velocities.
#
#
#  @subsection sec_usg Usage

import utime
import pyb
from pyb import I2C
from lab4 import IMU
from lab2 import MotorEncoder
from lab1 import MotorDriver






class potentiometerController:
    '''This class creates a potentiometer controller for use with the ME 405 Project. The controller returns an angle depending
       on potentiometer inputs. The class is equipped to handle variable gains for each direction, and has maximum and mimimum
       angles to prevent damage to the physical system.'''
    
    def __init__ (self, gainY = .3, gainX = .3, maxXangle = 30, minXangle = -30, maxYangle = 30, minYangle = -30, adcPin0 = pyb.Pin.cpu.B0, adcPin1 = pyb.Pin.cpu.C0):
        ''' Initilizes the controller with a variety of constants. All constants have desired values set as default. 
        @param gainY A float value used to set the proportional gain of the controller in the Y direction.
        @param gainX A float value used to set the proportional gain of the controller in the X direction.
        @param maxXangle A value used to determine the maximum X angle that can be returned.
        @param maxYangle A value used to determine the maximum Y angle that can be returned.
        @param minXangle A value used to determine the minimum X angle that can be returned.
        @param minYangle A value used to determine the minimum Y angle that can be returned.
        @param adcPin0 A pyb.Pin object containing one of the potentiometers ADC pins.
        @param adcPin1 A pyb.Pin object containing the other potentiometers ADC pin.
        '''
        
        #initialize all variables so that they can be used in functions
        self.maxXangle = maxXangle
        self.minXangle = minXangle
        self.maxYangle = maxYangle
        self.minYangle = minYangle
        self.gainX = gainX
        self.gainY = gainY
    
        self.adc0 = pyb.ADC(adcPin0)
        self.adc1 = pyb.ADC(adcPin1)
        
        #The default value of the potentiometer is shifted by half of the maximum to sign it. The reading on initilization is
        #then stored as an offset, assuming that the system is initilized with the potentiometer stationary.
        self.offsetX = self.adc0.read() - 2048
        self.offsetY = self.adc1.read() - 2048

    
    def updateDesiredY(self, desiredY):
    '''This function returns an angle that represents the desired Y angle. The function takes in the current angle,
    reads the potentiometer, and updates the angle based on potentiometer input.'''
        
        #read the potentiometer value for the Y direction, and correct for offset
        potY = (self.adc1.read() - 2048 - self.offsetY)
        
        #Threshold of 30 is used to account for variation in the potentiometer signal.
        if (potY < -30 or potY > 30):
            
            #increase the desired angle by the potentiometer reading (signed) times the gain
            desiredY += (potY/2048)*self.gainY
            
            #Prevent the desired angle from being outside the maximum and minimum.
            if (desiredY < self.minYangle):
                desiredY = self.minYangle
            if (desiredY > self.maxYangle):
                desiredY = self.maxYangle
        
        #Return the desired value
        return (desiredY)
        
    def updateDesiredX(self, desiredX):
    '''This function returns an angle that represents the desired X angle. The function takes in the current angle,
    reads the potentiometer, and updates the angle based on potentiometer input.'''
        
        #read the potentiometer value for the X direction, and correct for offset
        potX = (self.adc0.read() - 2048 - self.offsetX)
        
        #Threshold of 30 is used to account for variation in the potentiometer signal.
        if (potX < -30 or potX > 30):
            
            #increase the desired angle by the potentiometer reading (signed) times the gain
            desiredX += (potX/2048)*self.gainX
            
            #Prevent the desired angle from being outside the maximum and minimum.
            if (desiredX < self.minXangle):
                desiredX = self.minXangle
            if (desiredX > self.maxXangle):
                desiredX = self.maxXangle
        
        #Return the desired value
        return (desiredX)



class PControllerAngle:
    ''' This class implements a closed loop proportional controller for the
    ME405 board. '''

    def __init__ (self, KpropAngle):
        ''' Initilizes the controller with a proportional constant.
        @param KpropAngle A float value used to set the proportional gain of the controller.
        '''
        print ('Creating a closed loop proportional controller')
        self.Kp = KpropAngle
        
          
    def update (self, desired_angle, actual_angle):
        '''This function returns the actuation signal used to control the motor. The function
        takes in a desired angle, and actual angle. Update should be called at least every 10ms. The returned actuation
        signal is between -100 and 100, and should be used as the motor's duty cycle.'''
        
        #define error as actual position - desired position
        error = (desired_angle - actual_angle)
        
        #create an actuation signal based on the error and Kp. This represents the motor duty cycle
        act_signal = -error * self.Kp
        
        #duty cycle must be between -100 and 100, so ensure -100<actuation signal<100
        maxval = 100
        if(act_signal > maxval):
            act_signal = maxval
        if(act_signal < -maxval):
            act_signal = -maxval
        
        return(act_signal)



if __name__ == '__main__':
    '''This is the main method which controls the phone gimbal. '''
    
    # Create the pin objects used for interfacing with encoder A and B
    pin_ENCA_1 = pyb.Pin (pyb.Pin.cpu.B6)
    pin_ENCA_2 =  pyb.Pin (pyb.Pin.cpu.B7)
    pin_ENCB_1 = pyb.Pin (pyb.Pin.cpu.C6)
    pin_ENCB_2 =  pyb.Pin (pyb.Pin.cpu.C7)
    
    # Create the pin objects used for interfacing with motors A and B
    pin_EN1 = pyb.Pin (pyb.Pin.cpu.A10)
    pin_INA1 = pyb.Pin (pyb.Pin.cpu.B4)
    pin_INA2 =  pyb.Pin (pyb.Pin.cpu.B5)
    pin_EN2 = pyb.Pin (pyb.Pin.cpu.C1)
    pin_INB1 = pyb.Pin (pyb.Pin.cpu.A0)
    pin_INB2 =  pyb.Pin (pyb.Pin.cpu.A1)
    
    # Create the timer objects used for the motor encoders
    tim4 = pyb.Timer(4)
    tim8 = pyb.Timer(8)
    
    # Create the timer object used for PWM generation
    tim3 = pyb.Timer(3)
    tim5 = pyb.Timer(5)
    
    #Create the pin objects for ADC
    adc0 = pyb.Pin.cpu.B0
    adc1 = pyb.Pin.cpu.C0
    
    #Use variable Collect Data to decide if data will be printed for a timed run. 0=no, 1=yes
    collectData = 0
    
    # Create encoder objects, passing in the pins and timers
    Enc1 = MotorEncoder(pin_ENCA_1, pin_ENCA_2, tim4)
    Enc2 = MotorEncoder(pin_ENCB_1, pin_ENCB_2, tim8)
    
    # Create motor objects passing in the pins and timers
    Motor1 = MotorDriver(pin_EN1, pin_INA1, pin_INA2, tim3)
    Motor2 = MotorDriver(pin_EN2, pin_INB1, pin_INB2, tim5)
    
    #create the IMU object passing in the i2c port
    imu1 = IMU(1)
    
    #create the potentiometer controller object, passing in the 2 ADC pins
    pot1 = potentiometerController(adcPin0 = adc0,adcPin1 = adc1)
    
    #disable the motors and IMU before starting
    Motor1.disable()
    Motor2.disable()
    imu1.disable()
    
    #confirm that the user is ready to start
    while True:
        entry = input('press y to start: ')
        if entry == 'y':
            print("starting program")
            break
        else:
            pass
        
    
    #set the starting time now that the test has begun
    startTime = utime.ticks_ms()
    
    #set the starting angles the controller will try to reach
    desired_angleX = 0
    desired_angleY = 0
    
    #reset the encoder positions to 0
    Enc1.set_abs_position(0)
    Enc2.set_abs_position(0)
    
    #enable the motors and IMU
    imu1.enable()
    Motor1.enable()
    Motor2.enable()
    
    #create the angular controllers for the two motors with their respective gains
    CLAPX = PControllerAngle(2.5)
    CLAPY = PControllerAngle(3)
    
    #Set up the framework for collected data
    desiredAngleDataX = []
    desiredAngleDataY = []
    angleDataX = []
    angleDataY = []
    time_data = []
    encoderDataX = []
    encoderDataY = []
    BaseAngleX = []
    BaseAngleY = []
    

    #Begin the main loop
    while True:
        
        #update current time, and current IMU angle
        current_time = utime.ticks_ms()-startTime
        current_angle = imu1.getEULTuple()
        
        #update the desired angle based on potentiometer readings
        desired_angleX = pot1.updateDesiredX(desired_angleX)
        desired_angleY = pot1.updateDesiredY(desired_angleY)
        
        #Update encoder position
        Enc1.update()
        Enc2.update()
        
        #update the current position from the new encoder information
        currentPosXdeg = -Enc1.get_abs_position()*(360/(6900))
        currentPosYdeg = Enc2.get_abs_position()*(360/(6500))
        
        #Update the collected data approx every 25ms
        if(current_time % 10 == 0 and collectData == 1):
            time_data.append(current_time)
            angleDataX.append(int(current_angle[0]))
            angleDataY.append(int(current_angle[1]))
            encoderDataX.append(int(currentPosXdeg))
            encoderDataY.append(int(currentPosYdeg))
            desiredAngleDataX.append(int(desired_angleX))
            desiredAngleDataY.append(int(desired_angleY))
            BaseAngleY.append(int(currentPosYdeg-current_angle[1]))
            BaseAngleX.append(int(currentPosXdeg-current_angle[0]))
        
        #Get actuation signals
        act_sigX = CLAPX.update(desired_angleX,current_angle[0])
        act_sigY = CLAPY.update(desired_angleY,current_angle[1])
        
        #Turn on motors
        Motor1.set_duty(-act_sigX)
        Motor2.set_duty(act_sigY)
        
        
        #End loop after 9 seconds, and print the data
        if(current_time > 9000 and collectData == 1):
            Motor1.disable()
            Motor2.disable()
            for i in range(len(time_data)):
                print(time_data[i], ", ",angleDataX[i], ", ",angleDataY[i], ", ", BaseAngleX[i],", ", BaseAngleY[i])

            break
