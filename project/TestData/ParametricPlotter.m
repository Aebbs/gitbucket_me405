clear all;
close all;

data = readmatrix('data1.txt','Delimiter',',');

plot = 2; %1 for X     2 for Y

minXangle = min(min(data(:,2)),min(data(:,4)));
maxXangle = max(max(data(:,2)),max(data(:,4)));
minYangle = min(min(data(:,3)),min(data(:,5)));
maxYangle = max(max(data(:,3)),max(data(:,5)));

set(gcf, 'Position',  [0, 200, 900, 300]);
grid on;

if plot == 1
    Xgimbal= animatedline('Color','#FF3333' );
    Xbase = animatedline('Color', '#8C0000');
    set(gca,'XLim', [0,10000], 'Ylim', [minXangle,maxXangle]);
    legend('Gimbal Angle X','Base Angle X');
    pause(10);
    addpoints(Xgimbal,data(1,1),data(1,2));
    addpoints(Xbase,data(1,1),data(1,4));
end

if plot == 2
	Ygimbal= animatedline('Color','#3333FF' );
	Ybase = animatedline('Color', '#00008C');
    set(gca,'XLim', [0,10000], 'Ylim', [minYangle,maxYangle]);
    legend('Gimbal Angle Y','Base Angle Y');
    pause(10);
    addpoints(Ygimbal,data(1,1),data(1,3));
    addpoints(Ybase,data(1,1),data(1,5));
end
    

pause(data(1,1)/1000)

for i=2:length(data(:,1))
    if plot == 1
        addpoints(Xgimbal,data(i,1),data(i,2));
        addpoints(Xbase,data(i,1),data(i,4));
    end
    if plot == 2
    	addpoints(Ygimbal,data(i,1),data(i,3));
    	addpoints(Ybase,data(i,1),data(i,5));
    end
    drawnow limitrate;
    pause((data(i,1)-data(i-1,1))/1000);
end


%video 1 link:  https://youtu.be/ODYRmBFHYLA
%video 2 link:  https://youtu.be/6xZDhu7DaZs