## @file lab2.py
#  This is the Motor Encoder class for the ME405 code
#
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This file contains the Motor Encoder Class, which represents the work done in "lab2"
#  for the class ME 405. <br>
#  <a href="https://aebbs.bitbucket.io/projectMainpage/">Click HERE to return to the project mainpage</a>
#
#
#  @section sec_enc Motor Encoder
#  The motor encoder is used to track the position of the motor as it rotates. It includes
#  methods to update the position of the motor, find the absolute position of the motor,
#  and to find the change in position since the last update.
#
#  @section sec_usg Usage
#  A the constructor takes in 3 parameters: pins for encoder channels A and B, and a timer
#  To create a motor encoder object use: <br>
#  MotorEncoder(pin_ENC1_A, pin_ENC1_B, tim)
#
#  @section sec_tst Testing
#  This code was tested on the ME405 development board using connecting two motors and encoders.
#  the motors were spun by hand to confirm both encoders functioned as exected
#
#  @section sec_lim Limitations
#  This code has not been tested simultaneously with the motor drivers. Motors have not been
#  powered on or enabled during testing.
#
#  @author Anthony Ebbs
#
#  @copyright This software is open-source
#
#  @date 5/5/2020
#

import time
import pyb

class MotorEncoder:
    ''' This class implements a motor encoder for the
    ME405 board. '''
    
    def __init__ (self, ENC_A_pin, ENC_B_pin, tim):
        ''' Initializes the motor encoder pins and timer. 
        @param ENC_A_pin: A pyb.Pin object to use as the first encoder channel.
        @param ENC_B_pin: A pyb.Pin object to use as the second encoder channel.
        @param tim: A pyb.Timer object to use as the timer for the encoder.
        @param prev_pos: A variable used to store the most recently updated position of the motor
        @param abs_pos: A variable used to store the absolute position of the motor
        @param threshold: A variable used to determine how large of a change indicates that the
            timer has overflowed. THis value must be increased the less often update() is called '''
        
        #Creating the encoder pins as class variables
        self.ENC_A_pin = ENC_A_pin
        self.ENC_B_pin = ENC_B_pin
        
        #Initialize the timer
        tim.init(prescaler=0, period=65535)
        self.tim = tim
        
        # Create two channels using the time in encoder counting mode. These channels
        # also initialize the alternate function associated with each pin
        self.timer_ch1 = tim.channel(1, mode = pyb.Timer.ENC_AB, pin = ENC_A_pin)
        self.timer_ch1 = tim.channel(1, mode = pyb.Timer.ENC_AB, pin = ENC_B_pin)
        
        # Create class variables for the absolute position, previous position, and threshold
        self.prev_pos = 0
        self.abs_pos = 0
        self.threshold = 1000
        
    def update (self):
        '''Updates the previous position and absolute position of the motor'''
        self.abs_pos = self.abs_pos + self.get_delta()
        self.prev_pos = self.tim.counter()

    def get_current_position (self):
        '''Returns the current position of the motor'''
        return(self.tim.counter())
    
    def get_abs_position (self):
        '''Returns the absolute position of the motor'''
        return(self.abs_pos)
    
    def set_abs_position (self, pos_set):
        '''Sets the absolute position to the inputted value'''
        self.abs_pos = pos_set
    
    def get_delta (self):
        '''Returns the a delta value. This value indicates how far the motor has
            moved since the last call of update()'''
        
        #First check to see what the delta value is
        delta = self.tim.counter() - self.prev_pos
       
        #If the delta value has changed by a large amount (greater than "threshold")
        #we assume that the timer must have overflowed. The delta value must be corrected
        
        if (abs(delta)<self.threshold):
            delta = delta
        
        #Correct for the case of underflow
        elif(delta>0):
            delta = (self.tim.counter() - (self.prev_pos + 65535))
        
        #Correct for the case of overflow
        else:
            delta = ((self.tim.counter() + 65535) - self.prev_pos) 
        return(delta)
    
if __name__ == '__main__':
    ''' The main method of the code tests the motor driver class by
    enabling the motor, setting the duty cycle to 10%, letting the
    motor run for 5 seconds, then disabling the motor'''

    
    # Create the pin objects used for interfacing with two encoders
    pin_ENC1_A = pyb.Pin (pyb.Pin.cpu.B6)
    pin_ENC1_B =  pyb.Pin (pyb.Pin.cpu.B7)
    
    pin_ENC2_A = pyb.Pin (pyb.Pin.cpu.C6)
    pin_ENC2_B =  pyb.Pin (pyb.Pin.cpu.C7)
    
    # Create the timer objects used for encoder counting mode
    tim4 = pyb.Timer(4) 
    tim8 = pyb.Timer(8) 

    # Create an encoder object, passing in the pins and timer
    Enc1 = MotorEncoder(pin_ENC1_A, pin_ENC1_B, tim4)
    Enc2 = MotorEncoder(pin_ENC2_A, pin_ENC2_B, tim8)

    #disable the motor
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10) #why doesnt this disable
    pin_EN.init(mode = pyb.Pin.OUT_PP)
    pin_EN.low()
    
    #continuously update the motor's position and display absolute position
    while True:
        print("delta=",Enc1.get_delta()) 
        Enc1.update()
        print("abs=",Enc1.get_abs_position()) 
        time.sleep(0.1)


