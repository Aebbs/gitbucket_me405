## @file PotController.py
#  This is the potentiometer controller for the ME 405 project 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This file contains the PotController class, which is used to interpret the signals from the joystick's 2 potentiometers.
#  <a href="https://aebbs.bitbucket.io/projectMainpage/">Click HERE to return to the project mainpage</a>
#
#  @subsection sec_potCtrl PotController
#  The potentiometerController Class is used to find a desired IMU orientation angle based on user input of the joystick.
#  the contructor allows the user to initialize a gain in each direction, define bounds on the returned angle, and to
#  specify the ADC pins corresponding to each of the joystick's two potentiometers. 
#
#
#  @subsection sec_usg Usage
#  updateDesiredX(desiredX) and updateDesiredY(desiredY) are used to update the angles for each of the potentiometers.
#  Each method takes in the current desired angle, and returns a new desired angle based on the reading of the associated potentiometer.

class potentiometerController:
    '''This class creates a potentiometer controller for use with the ME 405 Project. The controller returns an angle depending
       on potentiometer inputs. The class is equipped to handle variable gains for each direction, and has maximum and mimimum
       angles to prevent damage to the physical system.'''
    
    def __init__ (self, gainY = .3, gainX = .3, maxXangle = 30, minXangle = -30, maxYangle = 30, minYangle = -30, adcPin0 = pyb.Pin.cpu.B0, adcPin1 = pyb.Pin.cpu.C0):
        ''' Initilizes the controller with a variety of constants. All constants have desired values set as default. 
        @param gainY A float value used to set the proportional gain of the controller in the Y direction.
        @param gainX A float value used to set the proportional gain of the controller in the X direction.
        @param maxXangle A value used to determine the maximum X angle that can be returned.
        @param maxYangle A value used to determine the maximum Y angle that can be returned.
        @param minXangle A value used to determine the minimum X angle that can be returned.
        @param minYangle A value used to determine the minimum Y angle that can be returned.
        @param adcPin0 A pyb.Pin object containing one of the potentiometers ADC pins.
        @param adcPin1 A pyb.Pin object containing the other potentiometers ADC pin.
        '''
        
        #initialize all variables so that they can be used in functions
        self.maxXangle = maxXangle
        self.minXangle = minXangle
        self.maxYangle = maxYangle
        self.minYangle = minYangle
        self.gainX = gainX
        self.gainY = gainY
    
        self.adc0 = pyb.ADC(adcPin0)
        self.adc1 = pyb.ADC(adcPin1)
        
        #The default value of the potentiometer is shifted by half of the maximum to sign it. The reading on initilization is
        #then stored as an offset, assuming that the system is initilized with the potentiometer stationary.
        self.offsetX = self.adc0.read() - 2048
        self.offsetY = self.adc1.read() - 2048

    
    
    def updateDesiredY(self, desiredY):
        '''This function returns an angle that represents the desired Y angle. The function takes in the current angle,
        reads the potentiometer, and updates the angle based on potentiometer input.'''
        
        #read the potentiometer value for the Y direction, and correct for offset
        potY = (self.adc1.read() - 2048 - self.offsetY)
        
        #Threshold of 30 is used to account for variation in the potentiometer signal.
        if (potY < -30 or potY > 30):
            
            #increase the desired angle by the potentiometer reading (signed) times the gain
            desiredY += (potY/2048)*self.gainY
            
            #Prevent the desired angle from being outside the maximum and minimum.
            if (desiredY < self.minYangle):
                desiredY = self.minYangle
            if (desiredY > self.maxYangle):
                desiredY = self.maxYangle
        
        #Return the desired value
        return (desiredY)
        
    def updateDesiredX(self, desiredX):
        '''This function returns an angle that represents the desired X angle. The function takes in the current angle,
        reads the potentiometer, and updates the angle based on potentiometer input.'''
        
        #read the potentiometer value for the X direction, and correct for offset
        potX = (self.adc0.read() - 2048 - self.offsetX)
        
        #Threshold of 30 is used to account for variation in the potentiometer signal.
        if (potX < -30 or potX > 30):
            
            #increase the desired angle by the potentiometer reading (signed) times the gain
            desiredX += (potX/2048)*self.gainX
            
            #Prevent the desired angle from being outside the maximum and minimum.
            if (desiredX < self.minXangle):
                desiredX = self.minXangle
            if (desiredX > self.maxXangle):
                desiredX = self.maxXangle
        
        #Return the desired value
        return (desiredX)
