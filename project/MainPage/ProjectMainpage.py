## @file ProjectMainpage.py
#  This is the mainpage for the ME405 Project
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is the project for the Cal Poly ME 405 class that was conducted by Anthony Ebbs. The goal of the project was to create
#  a two axis stabilizing gimbal designed for a phone.
#  
#
#  @subsection Project Proposal 
#  A project proposal was created at the beggining of the project to outline objectives, deliverables, timeline, and other
#  concerns such as safety. <br>
#  <a href="https://aebbs.bitbucket.io/htmlProposal">Click HERE to see the project proposal</a> 
#
#  @subsection sec_code Code
#  5 classes are imported to use in the gimbal's code: <br>
#
#  IMU: The IMU class is used to control the DFRobot 9dof IMU. This code was developed in Lab 4. <br>
#  <a href="https://aebbs.bitbucket.io/projectLab4/index.html">Click HERE for the IMU link </a>
#
#  MotorEncoder: The motor encoder class is used to read encoder signals from the two motors. This code was developed in Lab 2. <br>
#  <a href="https://aebbs.bitbucket.io/projectLab2/index.html">Click HERE for the MotorEncoder link</a>
#
#  MotorDriver: The motor driver class is used to send movement commands to the motors. This code was developed in Lab 1. <br>
#  <a href="https://aebbs.bitbucket.io/projectLab1/index.html">Click HERE for the MotorDriver link</a>
#
#  PcontrollerAngle: The PcontrollerAngle class is used to determine actuation signal from the current orientation of the gimbal. <br>
#  <a href="https://aebbs.bitbucket.io/projectAngleController/index.html">Click HERE for the PcontrollerAngle link</a>
#
#  potentiometerController: The potentiometer controller class is used to read signals from the joystick, which is composed of two potentiometers, and convert this
#  user input into a desired angular rotation. <br>
#  <a href="https://aebbs.bitbucket.io/projectPotController/index.html">Click HERE for the Potentiometer Controller link</a> 
#
#  All of the code used to control the system can be found here: <br>
#  <a href="https://aebbs.bitbucket.io/htmlLab2/index.html">Click HERE for code</a> 
#
#  @subsection sec_main Main Method
#  The main method used the 5 classes described above to control the gimbal. Pins, encoders, motors, IMU's, and all other objects
#  are instantiated with the propper pin locations, then a main control loop runs. If the user wants to collect data, the loop ends after
#  9 seconds and prints out the collected data, and if not, the loop runs indefinitely. Shown below is the main loop of the program. <br>
#  \code{.unparsed}
#      while True:
#        
#        #update current time, and current IMU angle
#        current_time = utime.ticks_ms()-startTime
#        current_angle = imu1.getEULTuple()
#        
#        #update the desired angle based on potentiometer readings
#        desired_angleX = pot1.updateDesiredX(desired_angleX)
#        desired_angleY = pot1.updateDesiredY(desired_angleY)
#        
#        #Update encoder position
#        Enc1.update()
#        Enc2.update()
#        
#        #update the current position from the new encoder information
#        currentPosXdeg = -Enc1.get_abs_position()*(360/(6900))
#        currentPosYdeg = Enc2.get_abs_position()*(360/(6500))
#        
#        #Update the collected data approx every 25ms
#        if(current_time % 10 == 0 and collectData == 1):
#            time_data.append(current_time)
#            angleDataX.append(int(current_angle[0]))
#            angleDataY.append(int(current_angle[1]))
#            encoderDataX.append(int(currentPosXdeg))
#            encoderDataY.append(int(currentPosYdeg))
#            desiredAngleDataX.append(int(desired_angleX))
#            desiredAngleDataY.append(int(desired_angleY))
#            BaseAngleY.append(int(currentPosYdeg-current_angle[1]))
#            BaseAngleX.append(int(currentPosXdeg-current_angle[0]))
#        
#        #Get actuation signals
#        act_sigX = CLAPX.update(desired_angleX,current_angle[0])
#        act_sigY = CLAPY.update(desired_angleY,current_angle[1])
#        
#        #Turn on motors
#        Motor1.set_duty(-act_sigX)
#        Motor2.set_duty(act_sigY)
#        
#        
#        #End loop after 9 seconds, and print the data
#        if(current_time > 9000 and collectData == 1):
#            Motor1.disable()
#            Motor2.disable()
#            for i in range(len(time_data)):
#                print(time_data[i], ", ",angleDataX[i], ", ",angleDataY[i], ", ", BaseAngleX[i],", ", BaseAngleY[i])
#
#            break
#  \endcode
#
#
#  @page Testing
#  Many tests were performed on the system, including tests of the motors, IMU, encoders, and potentiometer. Once individual
#  components of the system were shown to work, overall system tests were conducted. Two tests will be shown in this section, one
#  which was used to collect data over the 9 second time interval and one which showcases the use of the joystick.
#
#  @subsection sec_tst1 Test 1
#  The first test was used to collect data from the system, and show that the gimbal succesfully reduces motion. The system was manipulated
#  by hand, and 4 quantities were tracked with respect to time: The x and y angles of the center gimbal and the encoder measurements for the X and Y motors.
#  This data was processed using a matlab script to determine the rotation of the base, which was compared to the rotation of the center
#  gimbal. After the test, the matlab script was used to plot each of these rotations in real time, and the video footage was
#  stitched together alongside video footage of the gimbal.
#  <br>
#  <br>
#  <a href="https://youtu.be/ODYRmBFHYLA">Click HERE to view footage of Test1</a>
#
#  As can be seen in the video, this test proves that while the base of the gimbal undergoes changes from -30 deg to +30 deg,
#  the center of the gimbal only rotates approximately 5 deg in each direction. This indicates that the gimbal works correctly
#  and is able to damp much of the rotary motion. <br>
#
#
#  @subsection sec_tst2 Test 2
#  The second test was performed to show that the joystick can be used to control the system. Moving the joystick in each direction
#  allows the user to manually pan the motors and changes the setpoint of the system so that even after the joystick is released, the
#  gimbal remains in place. Furthermore, the base can still be rotated and the center of the gimbal will remain at the same non-flat orientation that
#  was commanded by the user.
#  <br>
#  <br>
#  <a href="https://youtu.be/6xZDhu7DaZs">Click HERE to view footage of Test2</a>
#
#  Test 2 shows that the joystick works correctly, and that the system can be manipulated and will still seek the
#  position orientation using the joystick
#
#  @page Results
#  Overall, the project was a success. The gimbal is able to drastically reduce angular rotation, and allows the user to manually
#  control the orientation using an external joystick. In my testing I did not put a phone within the gimbal because the added weight of the
#  phone drastically increased the load on my two motors. In a future revision, I would specifically size and select motors
#  to account for the weight and rotational intertia of the phone. Additionally, given more time I would incorporate the joystick
#  into the physical system and mount the entire device on a handle. Finally, a further improvement could be made by adding an
#  additional motor to control the Z axis: allowing for a fully functioning 3-axis gimbal.
#
#
#  @author Anthony Ebbs
#
#  @copyright This software is open-source
#
#  @date 6/7/2020
#

import utime
import pyb
from pyb import I2C
from lab4 import IMU
from lab2 import MotorEncoder
from lab1 import MotorDriver
from AngleController import PcontrollerAngle
from potController import potentiometerController


if __name__ == '__main__':
    '''This is the main method which controls the phone gimbal. '''
    
    # Create the pin objects used for interfacing with encoder A and B
    pin_ENCA_1 = pyb.Pin (pyb.Pin.cpu.B6)
    pin_ENCA_2 =  pyb.Pin (pyb.Pin.cpu.B7)
    pin_ENCB_1 = pyb.Pin (pyb.Pin.cpu.C6)
    pin_ENCB_2 =  pyb.Pin (pyb.Pin.cpu.C7)
    
    # Create the pin objects used for interfacing with motors A and B
    pin_EN1 = pyb.Pin (pyb.Pin.cpu.A10)
    pin_INA1 = pyb.Pin (pyb.Pin.cpu.B4)
    pin_INA2 =  pyb.Pin (pyb.Pin.cpu.B5)
    pin_EN2 = pyb.Pin (pyb.Pin.cpu.C1)
    pin_INB1 = pyb.Pin (pyb.Pin.cpu.A0)
    pin_INB2 =  pyb.Pin (pyb.Pin.cpu.A1)
    
    # Create the timer objects used for the motor encoders
    tim4 = pyb.Timer(4)
    tim8 = pyb.Timer(8)
    
    # Create the timer object used for PWM generation
    tim3 = pyb.Timer(3)
    tim5 = pyb.Timer(5)
    
    #Create the pin objects for ADC
    adc0 = pyb.Pin.cpu.B0
    adc1 = pyb.Pin.cpu.C0
    
    #Use variable Collect Data to decide if data will be printed for a timed run. 0=no, 1=yes
    collectData = 0
    
    # Create encoder objects, passing in the pins and timers
    Enc1 = MotorEncoder(pin_ENCA_1, pin_ENCA_2, tim4)
    Enc2 = MotorEncoder(pin_ENCB_1, pin_ENCB_2, tim8)
    
    # Create motor objects passing in the pins and timers
    Motor1 = MotorDriver(pin_EN1, pin_INA1, pin_INA2, tim3)
    Motor2 = MotorDriver(pin_EN2, pin_INB1, pin_INB2, tim5)
    
    #create the IMU object passing in the i2c port
    imu1 = IMU(1)
    
    #create the potentiometer controller object, passing in the 2 ADC pins
    pot1 = potentiometerController(adcPin0 = adc0,adcPin1 = adc1)
    
    #disable the motors and IMU before starting
    Motor1.disable()
    Motor2.disable()
    imu1.disable()
    
    #confirm that the user is ready to start
    while True:
        entry = input('press y to start: ')
        if entry == 'y':
            print("starting program")
            break
        else:
            pass
        
    
    #set the starting time now that the test has begun
    startTime = utime.ticks_ms()
    
    #set the starting angles the controller will try to reach
    desired_angleX = 0
    desired_angleY = 0
    
    #reset the encoder positions to 0
    Enc1.set_abs_position(0)
    Enc2.set_abs_position(0)
    
    #enable the motors and IMU
    imu1.enable()
    Motor1.enable()
    Motor2.enable()
    
    #create the angular controllers for the two motors with their respective gains
    CLAPX = PControllerAngle(2.5)
    CLAPY = PControllerAngle(3)
    
    #Set up the framework for collected data
    desiredAngleDataX = []
    desiredAngleDataY = []
    angleDataX = []
    angleDataY = []
    time_data = []
    encoderDataX = []
    encoderDataY = []
    BaseAngleX = []
    BaseAngleY = []
    

    #Begin the main loop
    while True:
        
        #update current time, and current IMU angle
        current_time = utime.ticks_ms()-startTime
        current_angle = imu1.getEULTuple()
        
        #update the desired angle based on potentiometer readings
        desired_angleX = pot1.updateDesiredX(desired_angleX)
        desired_angleY = pot1.updateDesiredY(desired_angleY)
        
        #Update encoder position
        Enc1.update()
        Enc2.update()
        
        #update the current position from the new encoder information
        currentPosXdeg = -Enc1.get_abs_position()*(360/(6900))
        currentPosYdeg = Enc2.get_abs_position()*(360/(6500))
        
        #Update the collected data approx every 25ms
        if(current_time % 10 == 0 and collectData == 1):
            time_data.append(current_time)
            angleDataX.append(int(current_angle[0]))
            angleDataY.append(int(current_angle[1]))
            encoderDataX.append(int(currentPosXdeg))
            encoderDataY.append(int(currentPosYdeg))
            desiredAngleDataX.append(int(desired_angleX))
            desiredAngleDataY.append(int(desired_angleY))
            BaseAngleY.append(int(currentPosYdeg-current_angle[1]))
            BaseAngleX.append(int(currentPosXdeg-current_angle[0]))
        
        #Get actuation signals
        act_sigX = CLAPX.update(desired_angleX,current_angle[0])
        act_sigY = CLAPY.update(desired_angleY,current_angle[1])
        
        #Turn on motors
        Motor1.set_duty(-act_sigX)
        Motor2.set_duty(act_sigY)
        
        
        #End loop after 9 seconds, and print the data
        if(current_time > 9000 and collectData == 1):
            Motor1.disable()
            Motor2.disable()
            for i in range(len(time_data)):
                print(time_data[i], ", ",angleDataX[i], ", ",angleDataY[i], ", ", BaseAngleX[i],", ", BaseAngleY[i])

            break

