## @file lab4.py
#  This is the IMU class for the ME 450 code
#
#
#  @mainpage
#  \tableofcontents
#
#  @section sec_intro Introduction
#  This file contains the IMU Class, which represents the work done in "lab4."
#  for the class ME 405. <br>
#  <a href="https://aebbs.bitbucket.io/projectMainpage/">Click HERE to return to the project mainpage</a>
#
#
#  @subsection sec_drv IMU
#  The IMU Class is used to interact with the DFRobot 9dof IMU provided in the ME 405 kit.
#  The class allows the user to enable and disable the imu, change the opperating mode,
#  and obtain various data including Euler Angles and angular velocities.
#
#
#  @subsection sec_usg Usage
#  To create an IMU object use: <br>
#  IMU(self, bus): 
#  With bus being a valid I2C bus <br><br>
#
#  The main functions of this class are as follows: <br>
#  To set the mode of the IMU use: def setmode(self, buff): with buff being an 8 bit integer or hexidecimal number representing the desired mode. <br><br>
#
#  To get angular velocity data use: getGyroTuple(self): Which will return a tuple of data including the angular velocities [X, Y, Z] <br><br>
#
#  To get Euler angles use: getEULTuple(self): Which will return a tuple of Euler angles [heading, roll, pitch]
#
#  @subsection sec_tst Testing
#  This code was tested on the ME405 development board, and was used to run a series of tests. 
#  Please see the "IMU Test" page in the documentation below for details on how the test was performed,
#  and for videos of the testing.
#
#
#  @subsection sec_lim Limitations
#  Only the NDOF mode was tested. Only bus 1 of the I2C was tested. 
#
#  @copyright This software is open-source
#
#  @date 5/5/2020
#
#  @page page1 IMU Test
#  This page details the  tests that were performed using the IMU class developed in lab4.
#
#  @section sec Tests Performed
#  6 tests were performed using the IMU to ensure the outputted tuples for Euler Angles and angular velocity were correct.
#  In the first 3 tests, the imu was positioned on top of a protractor, and was rotated 360 degrees along each axis.
#  The measured angles of rotation were compared to Euler angles to ensure the two agreed.
#  In the second 3 tests, the IMU was rotated approximately 180 degrees over a 1 second interval.
#  The angular acceleration data was monitored to ensure that the IMU reported values of approximately 180 degrees per second
#  over the entire rotation.
#
#
#  @subsection Test Results
#  The IMU unit successfully outputs accurate Euler angle data: With heading readings between 0-360 degrees, and pitch and roll readings between
#  -180 degrees and +180 degrees. <br>
#  While rotating about any of the three axis, the IMU accurately reported angular velocities of between 100-250 degrees per second. Because the IMU
#  was rotated by hand 180 deg over a 1 second interval, the fluctuation of this value is expected.
#
#  @subsection Testing Video
#  A video link of the Gyroscope test is linked below. <br>
#  https://youtu.be/U6JCxTR1rCM <br>
#  A video link of the Euler Angle test is linked below. <br>
#  https://youtu.be/f2uVLJUZA4o <br>

from pyb import I2C
import utime



class IMU:
    ''' This class implements an IMU for the ME405 board. '''
    
    def __init__ (self, bus):
        ''' Creates an IMU object using a given I2C bus
        @param bus An integer value used to select the I2C bus for the IMU to opperate on.
        '''
        i2c = I2C(bus, I2C.MASTER)
        self.i2c = i2c
        
    def scan(self):
        '''Uses the scan function to check if any I2C devices are properly connected. Returns the device adress of any connected devices'''
        return(self.i2c.scan())
    
    def setmode(self, buff):
        '''Sets the mode of the IMU.
        @param buff An integer or hex value used to select the mode. The values for each mode are described in table 3-5 of the BNO055 Datasheet
        '''
        self.i2c.mem_write(buff, 0x28, 0x3D)  #0x28 = I2c adress of BNO055, 0x3D = opr_mode register
    
    def getmode(self):
        '''Returns the current opperating mode of the IMU'''
        mode = bytearray(8)
        self.i2c.mem_read(mode, 0x28, 0x3D)   #0x28 = I2c adress of BNO055, 0x3D = opr_mode register
        return(mode)
    
    
    def getGyroDps(self,reglsb):
        '''The getGyroDps method returns the current reading of the gyroscope in degrees per second. The data is read from the
        least and most significant bits of the gyroscope registers, and scaled according to the BNO055 datasheet.
        @param reglsb: The method requires the user to pass in the starting adress of the gyroscope registers: corresponding to the lsb.
        '''
        #read data from i2c for 2 bytes: lsb and msb
        gyroData = self.i2c.mem_read(2, 0x28, reglsb) #1A=EULX lsb 1B=EULX msb
        
        #combine lsb and msb data into one 2byte number
        combG = (gyroData[0] | gyroData[1]<<8)
        
        #correctly sign the data to allow for negative values
        if(combG > 32767):
            combG = combG-65536
        
        #convert to degrees per second according to datasheet
        dps = combG/16
        return(dps)
    
    def getEULdeg(self,reglsb):
        '''The getEULdeg method returns the Eulerian position of the IMU in degrees. The data is read from the
        least and most significant bits of the EUL_DATA registers, and scaled according to the BNO055 datasheet.
        @param reglsb: The method requires the user to pass in the starting adress of the EUL_DATA registers: corresponding to the lsb.
        '''
        #read data from i2c for 2 bytes: lsb and msb
        EULdata = self.i2c.mem_read(2, 0x28, reglsb) #1A=EULX lsb 1B=EULX msb
        
        #combine lsb and msb data into one 2byte number
        combD = (EULdata[0] | EULdata[1]<<8)
        
        #correctly sign the data to allow for negative values
        if(combD > 32767):
            combD = combD-65536
            
        #convert to degrees per second according to datasheet
        dps = combD/16
        return(dps)
    
    def checkCalb(self):
        '''This method checks the calibration status of the IMU. The calibration status of each sensor is returned in the form:
        <br> [system status, gyroscope status, accelerometer status, magnometer status] <br>
        For each parameter, a value of 00 indicates the sensor is not calibrated and 03 indicates the sensor is calibrated'''
        
        #read teh calibration register data
        cal = self.i2c.mem_read(1, 0x28, 0x35)
        
        #parse the system calibration bits, and compare to 03
        sys = (cal[0] >> 6) & 0x03
        
        #parse the gyroscope calibration bits, and compare to 03
        gyro = (cal[0] >> 4) & 0x03
        
        #parse the accelerometer calibration bits, and compare to 03
        acl = (cal[0] >> 2) & 0x03
        
        #parse the magnometer calibration bits, and compare to 03
        mag = cal[0] & 0x03
        return(sys, gyro, acl, mag)
    
    
    def getGyroTuple(self):
        '''This method returns the gyroscope data (angular velocity) for the X,Y, and Z axis in degrees per second as a tuple'''
        
        #get gyro data for each axis: X register = 0x14, Y register = 0x16, Z register = 0x18
        gyroTuple = (self.getGyroDps(0x14),self.getGyroDps(0x16),self.getGyroDps(0x18))
        return(gyroTuple)
    
    def getEULTuple(self):
        '''This method returns the Eulerian data for the X,Y, and Z axis in degrees as a tuple'''
        
        #get Eulerian data for each axis: X register = 0x1E, Y register = 0x1C, Z register = 0x1A,
        EULTuple = (self.getEULdeg(0x1E),self.getEULdeg(0x1C),self.getEULdeg(0x1A))
        return(EULTuple)
    
    def enable(self):
        '''enable the IMU by setting it to NDOF mode'''
        imu1.setmode(0x0C)
    
    def disable(self):
        '''disable the IMU by setting it to config mode'''
        imu1.setmode(0x00)
    

    

if __name__ == '__main__':
    imu1 = IMU(1)
    utime.sleep_ms(100)
    imu1.setmode(0x0C)
    
    while True:
        print(imu1.getEULTuple())
        #print(imu1.checkCalb())
        utime.sleep_ms(100)

    



