import utime
import pyb


class potentiometerController:
    
    def __init__ (self, gainY = 2, gainX = 2, maxXangle = 10, minXangle = -10, maxYangle = 10, minYangle = -10, adcPin0 = pyb.Pin.cpu.A0, adcPin1 = pyb.Pin.cpu.A1):
        self.maxXangle = maxXangle
        self.minXangle = minXangle
        self.maxYangle = maxYangle
        self.minYangle = minYangle
        self.gainX = gainX
        self.gainY = gainY
    
        self.adc0 = pyb.ADC(adcPin0)
        self.adc1 = pyb.ADC(adcPin1)
        
        self.offsetX = self.adc0.read() - 2048
        self.offsetY = self.adc1.read() - 2048

    
    def updateDesiredY(self, desiredY):
        potY = (self.adc1.read() - 2048 - self.offsetY)
        if (potY < -20 or potY > 20):
            desiredY += (potY/2048)*self.gainY
            if (desiredY < self.minYangle):
                desiredY = self.minYangle
            if (desiredY > self.maxYangle):
                desiredY = self.maxYangle
        return (desiredY)
        
    def updateDesiredX(self, desiredX):
        potX = (self.adc0.read() - 2048 - self.offsetX)
        if (potX < -20 or potX > 20):
            desiredX += (potX/2048)*self.gainX
            if (desiredX < self.minXangle):
                desiredX = self.minXangle
            if (desiredX > self.maxXangle):
                desiredX = self.maxXangle
        return (desiredX)

if __name__ == '__main__':
    ''' description here'''

    startTime = utime.ticks_ms()
    desiredX = 0
    desiredY = 0
    pot1 = potentiometerController(gainX = 10)
    
    while True:
        currentTime = utime.ticks_ms()-startTime
        if(currentTime % 100 == 0):
            desiredX = pot1.updateDesiredX(desiredX)
            desiredY = pot1.updateDesiredY(desiredY)
            print("y:", desiredY)
            print("x:", desiredX)
            print()
    
                
        
        
    
