## @file mainpage.py
#  This is the mainpage for the ME405 code
#
#  This code is used for the development of code to control a motor. This includes
#  classes for motor driver and motor encoders.
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This project is created for the cal poly class ME 405, in which students breadboard
#  and develop code for small gearmotors with a nucleo microcontroller
#  
#
#  @section sec_mot Motor Driver
#  The motor driver class is used to create a motor driver, with functionality to
#  control the direction and speed of the motor. <br>
#  <a href="https://aebbs.bitbucket.io/htmlLab1/index.html">CLICK HERE FOR MOTOR DRIVER LINK</a> 
#
#  @section sec_enc Motor Encoder
#  The motor encoder is used to track the position of the motor as it rotates. It includes
#  methods to update the position of the motor, find the absolute position of the motor,
#  and to find the change in position since the last update.<br>
#  <a href="https://aebbs.bitbucket.io/htmlLab2/index.html">CLICK HERE FOR MOTOR ENCODER LINK</a> 
#
#
#  @section sec_cont Motor Controller
#  The motor controller is a closed loop, proportional controller used to provide an actuation signal to the motor. <br>
#  <a href="https://aebbs.bitbucket.io/htmlLab3/index.html">CLICK HERE FOR MOTOR CONTROLLER LINK</a>
#
#  @section sec_cont IMU
#  The IMU is used to collect data from the DFROBOT BNO055 10DOF IMU. <br>
#  <a href="https://aebbs.bitbucket.io/htmlLab4/index.html">CLICK HERE FOR IMU LINK</a> 
#
#
#  @author Anthony Ebbs
#
#  @copyright This software is open-source
#
#  @date 5/5/2020
#