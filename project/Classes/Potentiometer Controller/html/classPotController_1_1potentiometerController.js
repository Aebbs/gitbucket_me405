var classPotController_1_1potentiometerController =
[
    [ "__init__", "classPotController_1_1potentiometerController.html#aee9a1e83719810675f0da12c97d7f5a9", null ],
    [ "updateDesiredX", "classPotController_1_1potentiometerController.html#a15a19704138a56425b84bd21813a5453", null ],
    [ "updateDesiredY", "classPotController_1_1potentiometerController.html#a7af480a4c0f0debfd44c6cc0adc88ad1", null ],
    [ "adc0", "classPotController_1_1potentiometerController.html#afc120c3810ca1d7b1bde3ee8b2c10761", null ],
    [ "adc1", "classPotController_1_1potentiometerController.html#aa485ee11ceefa9df72044762c3e72e42", null ],
    [ "gainX", "classPotController_1_1potentiometerController.html#acc5753a758c5d74fa7dc7398071bdb11", null ],
    [ "gainY", "classPotController_1_1potentiometerController.html#aa76f16f6a7fcf947a9592b330cbe2503", null ],
    [ "maxXangle", "classPotController_1_1potentiometerController.html#aab280a333056fa2d23b036e74df63bef", null ],
    [ "maxYangle", "classPotController_1_1potentiometerController.html#afd97efa15c5c13784b95a2fe7410fb3b", null ],
    [ "minXangle", "classPotController_1_1potentiometerController.html#ab1d9cc517fdbe5f6e37c5d5faaad9827", null ],
    [ "minYangle", "classPotController_1_1potentiometerController.html#ad264879c47c9b0676ebd352e367ce2c0", null ],
    [ "offsetX", "classPotController_1_1potentiometerController.html#aecd24592202c95f68356d2f05508d9e2", null ],
    [ "offsetY", "classPotController_1_1potentiometerController.html#a8396f967072e0d6515df86a4a589e145", null ]
];