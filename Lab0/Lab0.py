''' @file Lab0.py
There must be a docstring at the beginning of a Python source file
with an @file [filename] tag in it! '''

def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
        Fibonacci number.
    @param fibNum a returned value corresponding to the Fibonacci number 
        at index idx'''
    
    #Fibonacci numbers are calculated recursively,
    if idx > 1:
        fibNum = fib(idx-1) + fib(idx-2)
    else:
        fibNum = idx

    return fibNum

''' Main function that prompts user to enter indices for fibonacci numbers'''
if __name__ == '__main__':
    
    #Ask user to input an index number
    while True:
        entry = input('Enter the index of a Fibonacci number you want to find: ')
        
        #Check to make sure index entry is an integer
        try:
            fibToFind = int(entry)
            #check to make sure the integer is positive
            if fibToFind >= 0:
                
                #if integer is positive, calculate the fibonacci number and print it
                print ('\n Calculating Fibonacci number at '
                       'index n = {:}.'.format(fibToFind))
                
                result = fib(fibToFind)
                
                #Print the resulting number
                print ('The Fibonacci Number at index {} is: {}'.format(fibToFind,result))
                
            #if integer is negative, prompt user to enter a positive number
            else:
                print('please enter a positive integer')
        
        #exception is raised if entry is not an integer. Prompt user to enter an int
        except ValueError:
            print ('\n Please enter a valid integer index')
            
        #Check to see if user wants to enter a new number
        userContinue = input('Enter "y" to select a new index or "n" to exit:' )
        if userContinue == 'y':
            pass
        else:
            print('exiting the program')
            break

            

    