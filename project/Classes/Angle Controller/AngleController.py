## @file AngleController.py
#  This is the angle controller for the ME 405 project 
#
#
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This file contains the angle controller class, which is used to control the angle of the gimbal in the X and Y directions.
#  <a href="https://aebbs.bitbucket.io/projectMainpage/">Click HERE to return to the project mainpage</a>
#
#  @subsection sec_angleCont AngleController
#  The AngleController Class is used to get an actuation signal based on the current orientation of the IMU. This actuation signal
#  is used as a duty cycle for the X and Y motors. 
#
#
#  @subsection sec_usg Usage
#  update (desired_angle, actual_angle) the main function of this class. It takes in the desired angle for the IMU, and the current orientation of the IMU, and
#  calculates a proportional actuation signal based on the gain of the controller.

class PControllerAngle:
    ''' This class implements a closed loop proportional controller for the
    ME405 board. '''

    def __init__ (self, KpropAngle):
        ''' Initilizes the controller with a proportional constant.
        @param KpropAngle A float value used to set the proportional gain of the controller.
        '''
        print ('Creating a closed loop proportional controller')
        self.Kp = KpropAngle
        
          
    def update (self, desired_angle, actual_angle):
        '''This function returns the actuation signal used to control the motor. The function
        takes in a desired angle, and actual angle. Update should be called at least every 10ms. The returned actuation
        signal is between -100 and 100, and should be used as the motor's duty cycle.'''
        
        #define error as actual position - desired position
        error = (desired_angle - actual_angle)
        
        #create an actuation signal based on the error and Kp. This represents the motor duty cycle
        act_signal = -error * self.Kp
        
        #duty cycle must be between -100 and 100, so ensure -100<actuation signal<100
        maxval = 100
        if(act_signal > maxval):
            act_signal = maxval
        if(act_signal < -maxval):
            act_signal = -maxval
        
        return(act_signal)


