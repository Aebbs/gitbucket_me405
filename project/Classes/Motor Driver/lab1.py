## @file lab1.py
#  This is the Motor Driver class for the ME405 code
#
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This file contains the Motor Driver Class, which represents the work done in "lab1"
#  for the class ME 405. <br>
#  <a href="https://aebbs.bitbucket.io/projectMainpage/">Click HERE to return to the project mainpage</a>
#
#
#  @section sec_drv Motor Driver
#  The motor driver is used to crontrol the position of one or more gearmotors connected to
#  nucleo development board. The class allows the user to enable and disable the motor,
#  and to set the duty cycle and direction of shaft rotation.
#
#  @section sec_usg Usage
#  A the constructor takes in 4 parameters: an enable pin, two input pins, and a timer
#  To create a motor driver object use: <br>
#  MotorDriver(pin_EN, pin_IN1, pin_IN2, tim1)
#
#  @section sec_tst Testing
#  This code was tested on the ME405 development board, and was used to run a motor
#  at a variety of duty cycles in both directions.
#
#  @section sec_lim Limitations
#  Only integer duty cycles above 15% were tested. This code was not tested with two motors
#  simultaneously
#
#  @copyright This software is open-source
#
#  @date 5/5/2020
#

import time
import pyb

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
            and IN2_pin. '''
        print ('Creating a motor driver')
        
        # Initialize pins as output with push-pull control
        EN_pin.init(mode = pyb.Pin.OUT_PP)
        IN1_pin.init(mode = pyb.Pin.OUT_PP)
        IN2_pin.init(mode = pyb.Pin.OUT_PP)
        
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        
        #initialize the timer
        timer.init(freq = 20000)
        
        # Create two channels to use with pulse_width_percent. These channels
        # Correspond to the appropriate pins on the board as outlined in the datasheet
        self.IN1_ch1 = timer.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.IN2_ch2 = timer.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)

    
    def enable (self):
        '''Enables motor by setting the EN_pin pin high'''
        print ('Enabling Motor')
        self.EN_pin.high ()
        

    def disable (self):
        '''Enables motor by setting the EN_pin pin low'''
        print ('Disabling Motor')
        self.EN_pin.low ()

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in the CCW direction, negative values
        in the CW direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
      
        # Duty must be a positive number: for negative duty values IN1 is set high
        # for positive duty values IN2 is set high
        if(duty <0 ):
            duty = abs(duty)
            self.IN1_ch1.pulse_width_percent(duty)
            self.IN2_ch2.pulse_width_percent(0)
        else:
            self.IN1_ch1.pulse_width_percent(0)
            self.IN2_ch2.pulse_width_percent(duty)


if __name__ == '__main__':
    ''' The main method of the code tests the motor driver class by
    enabling the motor, setting the duty cycle to 10%, letting the
    motor run for 5 seconds, then disabling the motor'''
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    
    # Create the pin objects used for interfacing with the motor driver 
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10)
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4)
    pin_IN2 =  pyb.Pin (pyb.Pin.cpu.B5)
    
    # Create the timer object used for PWM generation
    tim1 = pyb.Timer(3) #setting frequency above audible range 

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim1)


    # Set the duty cycle to 10 percent and enable
    moe.set_duty(50)
    moe.enable()
    
    # Sleep for 5 seconds
    print ('wait 5 seconds')
    time.sleep(5)
    
    # Disable the Motor
    moe.disable()
